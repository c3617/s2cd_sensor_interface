// device: NodeMCU 1.0 (ESP-12E Module)
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <DHT.h>

// add your WiFi name and password in conf.h file
#include "conf.h"

typedef struct ResponseObj
{
  int statusCode;
  String body; 
};


ESP8266WebServer server(80);

void log(HTTPMethod method, String uri, WiFiClient client);
void handle_trigger();
void handle_notfound();

WiFiClient client;
HTTPClient http;

ResponseObj httpGETRequest(String relPath);
ResponseObj httpPOSTRequest(String relPath, String body);

void sendRandomSensorValue();

#define DHTPIN D2
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

const int button = D0;

void sendSensorValue();

void setup() {
  Serial.begin(115200);
  delay(50);
  Serial.println();

  pinMode(button, INPUT);
  pinMode(DHTPIN, INPUT);

  dht.begin();

  WiFi.begin(ssid, pwd);


  Serial.print("Connecting");
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.print("Connected, local IP address: ");
  Serial.println(WiFi.localIP().toString());

  delay(1000);
  Serial.println();

  ResponseObj res = httpPOSTRequest("/remote_trigger_setup", "{ \"ip\": \"http://" + WiFi.localIP().toString() + "/\"}");
  Serial.println(res.statusCode);
  Serial.println(res.body);

  server.on("/remote_trigger", handle_trigger);
  server.onNotFound(handle_notfound);

  server.begin();
  Serial.print("http server started at http://");
  Serial.print(WiFi.localIP().toString());
  Serial.println(":80/");

  // sendRandomSensorValue();
}

void loop() {
  server.handleClient();

  if(digitalRead(button))
  {
    Serial.println("button pressed");
    sendSensorValue();
    delay(300);
  }
}

void log(HTTPMethod method, String uri, WiFiClient client){
  Serial.print(client.remoteIP().toString() + " ");
  Serial.print(uri);
  if(method == HTTP_GET)  {    
    Serial.println(" HTTP /GET");
  }
}

void handle_trigger()
{  
  log(server.method(), server.uri(), server.client());

  sendSensorValue();

  server.send(200, "application/json", "{\"msg\": \"trigger successful\"}");
}

void handle_notfound()
{
  log(server.method(), server.uri(), server.client());
  server.send(404, "text/plain", "Not Found");
}

ResponseObj httpGETRequest(String relPath)
{
  http.begin(client, apiUrl + relPath);
  int httpCode = http.GET();

  ResponseObj res;
  res.statusCode = httpCode;
  
  res.body = http.getString();

  http.end();
  
  return res;
}

ResponseObj httpPOSTRequest(String relPath, String body)
{
  http.begin(client, apiUrl + relPath);
  http.addHeader("content-type", "application/json");
  
  int httpCode = http.POST(body);

  ResponseObj res;
  res.statusCode = httpCode;
  res.body = http.getString();

  http.end();

  return res;
}

void sendRandomSensorValue()
{
  float value = random(0, 20.0);
  value = (int)(value * 100);
  value /= 100;
  ResponseObj res = httpPOSTRequest(
    "/sensor_values", 
    "{ \"value\":" + String(value) + "}"
  );
  Serial.println(res.statusCode);
  Serial.println(res.body);
}

void sendSensorValue()
{
  float value = dht.readTemperature();

  if(isnan(value))
  {
    Serial.println("Failed to read from DHT sensor!");
  }
  else
  {
    ResponseObj res = httpPOSTRequest(
      "/sensor_values",
      "{ \"value\": " + String(value) + "}"
    );
    Serial.println(res.statusCode);
    Serial.println(res.body);
  }
}