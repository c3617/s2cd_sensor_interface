# Sensor to Cloud DEMO - Sensor Interface

Setup
* install ESP8266 libraries in Arduino IDE
* select `NodeMCU 1.0 (ESP-12E Module)` board from Tools/Board in menu bar of Arduino IDE
* install `DHT sensor` library in Arduino IDE
* create a header file `conf.h` with the following code
    ```C++
        #ifndef _CONF_H
        #define _CONF_H

        // add your WiFi name and password here
        String ssid = "your_wifi_name";
        String pwd = "your_wifi_password";

        String apiUrl = "your_api_url";

        #endif
    ```
* connect your NodeMCU ESP8266 and upload the code

Arduino ESP8266 Documentation
* (ESP8266 wifi)[https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/readme.html]
* (ESP8266 http GET request)[https://github.com/douglaszuqueto/esp8266-http-request/blob/master/esp8266/get-by-id.ino]
* (ESP8266 http POST request)[https://github.com/douglaszuqueto/esp8266-http-request/blob/master/esp8266/post.ino]